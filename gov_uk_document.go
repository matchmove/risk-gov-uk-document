package govUKDocuments

import (
	"fmt"
	"log"
	"strings"

	"bitbucket.org/matchmove/risk-document"
	"bitbucket.org/matchmove/risk-interests"
	"bitbucket.org/matchmove/utils"
)

const (
	// GovUKDocumentSeparator separator for document gov.uk
	GovUKDocumentSeparator = ";"
	// SourceGovUK ...
	SourceGovUK = "www.gov.uk"
	// FetchSubSetDatasModeNumeric mode to get the
	// subset that is separated by numeric bullets
	FetchSubSetDatasModeNumeric = "numeric"
	// FetchSubSetDatasModeAlphaNumeric mode to get
	// the subset that is separated by alpha numeric bullets
	FetchSubSetDatasModeAlphaNumeric = "alpha"
	// FetchSubSetDatasSeparatorColon colon separator
	FetchSubSetDatasSeparatorColon = ":"
	// FetchSubSetDatasSeparatorCloseParenthesis close parenthesis separator
	FetchSubSetDatasSeparatorCloseParenthesis = ")"
)

// GovUKDocument represent the file to be process
type GovUKDocument struct {
	documents.DocumentObject
	Reference   string
	LastUpdated string
	Headers     []string
	Individuals []map[string]string
	Entities    []map[string]string
}

// NewGovUKDocument initilized GovUKDocument
func NewGovUKDocument(f documents.File) GovUKDocument {
	f.ReadFile()

	document := GovUKDocument{}
	document.Body = f.Contents
	document.Reference = f.Path + f.Filename
	return document
}

// Parse breaks d.Body into arrays
func (d *GovUKDocument) Parse() {

	defer func() {
		if recovery := recover(); recovery != nil {
			log.Println("Recovering from GovUKDocument.Parse Exception:", recovery)
		}
	}()

	slice := strings.Split(d.Body, "\n")

	for _, line := range slice {
		if len(line) == 0 {
			continue
		}

		if d.LastUpdated == "" {
			d.LastUpdated = line[strings.Index(line, GovUKDocumentSeparator):]
			continue
		}

		if len(d.Headers) == 0 {
			d.Headers = strings.Split(line, GovUKDocumentSeparator)
		} else {

			currentLine := strings.Split(line, GovUKDocumentSeparator)

			interest := make(map[string]string)
			for ctr, value := range currentLine {
				interest[utils.SlugReplace(d.Headers[ctr], "_")] = utils.TrimNewlineSpaces(value)
			}

			if interest["group_type"] == interests.InterestTypeIndividual {
				d.Individuals = append(d.Individuals, interest)
			}

			if interest["group_type"] == interests.InterestTypeEntities {
				d.Entities = append(d.Entities, interest)
			}
		}
	}

}

// Format parse the array in d.Entities and d.Individuals
// and assigns them to an array of Interest
func (d *GovUKDocument) Format() interests.Interests {
	defer func() {
		if recovery := recover(); recovery != nil {
			log.Println("Recovering from GovUKDocument.Format Exception:", recovery)
		}
	}()

	var interests interests.Interests

	if len(d.Individuals) > 0 {
		for _, data := range d.Individuals {
			interest := d.ParseGovUKData(data)
			interests = append(interests, interest)
		}
	}

	if len(d.Entities) > 0 {
		for _, data := range d.Entities {
			interest := d.ParseGovUKData(data)
			interests = append(interests, interest)
		}
	}

	return interests
}

// ParseGovUKData parses the body and assigns them to the correct fields
func (d GovUKDocument) ParseGovUKData(data map[string]string) interests.Interest {
	defer func() {
		if recovery := recover(); recovery != nil {
			log.Println("Recovering from GovUKDocument.ParseGovUKData Exception:", recovery)
		}
	}()

	var documents []string

	if data["passport_details"] != "" {
		documents = append(documents, "Passport:"+data["passport_details"])
	}

	if data["ni_number"] != "" {
		documents = append(documents, "National ID:"+data["ni_number"])
	}
	return interests.Interest{
		ID:               "",
		Name:             data["name_1"],
		Source:           SourceGovUK,
		Reference:        d.Reference,
		AliasGoodQuality: ([]string{data["name_2"], data["name_3"], data["name_4"], data["name_5"], data["name_6"]}),
		Type:             data["group_type"],
		Title:            data["title"],
		POBTown:          data["town_of_birth"],
		POBCountry:       data["country_of_birth"],
		Country:          ([]string{data["country"], data["country_of_birth"]}),
		Address:          ([]string{data["address_1"], data["address_2"], data["address_3"], data["address_4"], data["address_5"], data["address_6"], data["post/zip_code"]}),
		OtherInformation: data["other_information"],
		ListedOn:         data["listed_on"],
		LastUpdated:      data["last_updated"],
		Regime:           data["regime"],
		Documents:        documents,
		Designation:      data["position"],
		DOB:              []string{data["dob"]},
		Nationality:      FetchSubSetDatas(data["nationality"], FetchSubSetDatasModeNumeric, FetchSubSetDatasSeparatorCloseParenthesis),
	}
}

// FetchSubSetDatas get the subset of the
// content that is identified by the a bullet
func FetchSubSetDatas(content string, mode string, separator string) []string {
	defer func() {
		if recovery := recover(); recovery != nil {
			log.Println("Recovering from GovUKDocument.FetchSubSetDatas Exception:", recovery)
		}
	}()

	var results []string
	var (
		ctrStart  int
		ctrEnd    int
		ctrFormat string
		start     string
		end       string
		subset    string
	)

	switch mode {
	case FetchSubSetDatasModeNumeric:
		ctrStart = 1
		ctrEnd = 7
		ctrFormat = "%d%s"
		break
	case FetchSubSetDatasModeAlphaNumeric:
		ctrStart = 97
		ctrEnd = 107
		ctrFormat = "%c%s"
		break
	default:
		panic("Invalid FetchSubSetDatas Mode")
	}

	for ctr := ctrStart; ctr <= ctrEnd; ctr++ {
		subset = "na"
		start = fmt.Sprintf(ctrFormat+" ", ctr, separator)

		if ctr > ctrStart {
			start = " " + start
			if !strings.Contains(content, start) {
				return results
			}
		}

		if strings.Contains(content, start) {
			end = fmt.Sprintf(" "+ctrFormat+" ", ctr+1, separator)

			if strings.Contains(content, end) {
				subset = FetchUKGovData(content, start, end, "")
			} else {
				subset = FetchUKGovData(content, start, "", "")
			}
		}

		if (ctr == ctrStart) && !strings.Contains(content, start) {
			subset = content
		}

		if subset != "na" {
			results = append(results, subset)
		}
	}

	return results
}

// FetchUKGovData get the data from data
func FetchUKGovData(body string,
	start string,
	end string,
	fallback string) string {
	iStart := strings.Index(body, start) + len([]rune(start))
	iEnd := strings.Index(body[iStart:], end) + iStart
	var subset string

	if strings.Contains(body, start) && end == "" {

		subset = utils.TrimNewlineSpaces(body[iStart:])

	} else {

		if strings.Contains(body, start) && strings.Contains(body, end) {
			subset = utils.TrimNewlineSpaces(body[iStart:iEnd])

		} else if strings.Contains(body, start) &&
			strings.Contains(body, fallback) {

			iEnd = strings.Index(body[iStart:], fallback) + iStart
			subset = utils.TrimNewlineSpaces(body[iStart:iEnd])
		}

	}

	if subset != "na" {
		return subset
	}

	return ""
}
