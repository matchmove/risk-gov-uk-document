package govUKDocuments

import (
	"fmt"
	"reflect"
	"strings"
	"testing"

	documents "bitbucket.org/matchmove/risk-document"
)

const (
	ProcessorSource = "documents/source/"
)

func TestNewGovUKDocument(t *testing.T) {
	files, err := documents.GetLatestFiles(ProcessorSource)
	if err != nil {
		t.Errorf("Exception occured in files.GetLatest: %s", err)
	}

	for _, file := range files {
		if strings.Contains(strings.ToLower(file.Filename), SourceGovUK) {

			document := NewGovUKDocument(file)

			if "main.GovUKDocument" != fmt.Sprint(reflect.TypeOf(document)) {
				t.Errorf("NewGovUKDocument instance expecting:%s, got:%s", "main.GovUKDocument", string(fmt.Sprint(reflect.TypeOf(document))))
			}

			document.Parse()
			document.Format()
		}
	}
}
